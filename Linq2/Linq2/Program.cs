﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq2
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] cities = {"Rome", "London", "Nairobi", "California", "Delhi", "Zurich", "Amsterdam", "Abu Dhabi", "Paris"};
            

            var goodCities= from item in cities
                                  where item.StartsWith("A") && item.EndsWith("m")
                                  select item;

            

            foreach (var item in goodCities)
            {
                Console.WriteLine(item);
            }

            while (true) ;
        }
    }
}
